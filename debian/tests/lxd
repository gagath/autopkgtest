#!/bin/sh
set -eu
arch=$(dpkg --print-architecture)
if [ "$arch" != i386 ] && [ "$arch" != amd64 ]; then
    # we don't have LXD images for most non-x86 architectures
    echo "Skipping on non-x86 architecture $arch"
    exit 77
fi

if [ -z "${AUTOPKGTEST_NORMAL_USER-}" ]; then
    echo "Skipping test because it requires an AUTOPKGTEST_NORMAL_USER"
    exit 77
fi

# On Ubuntu LXD is provided as a snap. Install snapd if not present.
if dpkg-vendor --is ubuntu && ! command -v snap >/dev/null; then
    DEBIAN_FRONTEND=noninteractive apt-get -q --yes install snapd
fi

if [ -z "${AUTOPKGTEST_TEST_UNINSTALLED-}" ]; then
    export AUTOPKGTEST_TEST_INSTALLED=yes
    build_lxd=autopkgtest-build-lxd
else
    build_lxd=tools/autopkgtest-build-lxd
fi

# Detect LXD API extensions
lxd_extension() {
    lxc info | grep -q "^\- ${1}$"
}

lxd waitready
lxd init --minimal
lxd init --dump
adduser "$AUTOPKGTEST_NORMAL_USER" lxd

[ -n "${http_proxy:-}" ] && lxc config set core.proxy_http "${http_proxy}"
[ -n "${https_proxy:-}" ] && lxc config set core.proxy_https "${https_proxy}"

# work around broken PTMU in Canonical Scalingstack
iptables -t mangle -A FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu

if dpkg-vendor --is ubuntu; then
    remote=ubuntu
    vendor=ubuntu
    suite=$(ubuntu-distro-info --lts)
    image="$remote:$suite/$arch"
else
    remote=images
    vendor=debian
    suite="$(debian-distro-info --testing)"
    image="$remote:$vendor/$suite/$arch"
fi

# workaround for #1071047
if dpkg --compare-versions "$(lxd version | cut -d ' ' -f 1)" lt 5.21.1; then
    runuser -u "$AUTOPKGTEST_NORMAL_USER" -- lxc remote set-url images https://images.lxd.canonical.com
fi

runuser -u "$AUTOPKGTEST_NORMAL_USER" -- \
"$build_lxd" "$image"

exec annotate-output \
runuser -u "$AUTOPKGTEST_NORMAL_USER" -- \
env \
    AUTOPKGTEST_TEST_INSTALLED="${AUTOPKGTEST_TEST_INSTALLED-}" \
    AUTOPKGTEST_TEST_UNINSTALLED="${AUTOPKGTEST_TEST_UNINSTALLED-}" \
    AUTOPKGTEST_TEST_LXD="autopkgtest/$vendor/$suite/$arch" \
./tests/autopkgtest LxdRunner
